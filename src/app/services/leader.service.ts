import { Injectable } from '@angular/core';
import { Leader } from './../shared/leader';
import { LEADERS } from './../shared/leaders';
import { Observable,of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {baseURL} from '../shared/baseurl';

@Injectable({
   providedIn: 'root'
 })
 export class LeaderService {

   constructor(private http: HttpClient) { }

   getLeaders(): Observable<Leader[]> {
    return this.http.get<Leader[]>(baseURL + 'leadership');

}

   getLeaderFeatured(): Observable<Leader> {
    return of (LEADERS.filter(leader => leader.featured)[0]).pipe(delay(2000));
   }
 }



